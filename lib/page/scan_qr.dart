import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
//import 'package:share_plus/share_plus.dart';
import 'package:simple_barcode_scanner/simple_barcode_scanner.dart';

class QR extends StatefulWidget {
  const QR({super.key});

  @override
  State<QR> createState() => _QRState();
}

class _QRState extends State<QR> {
  String? qrResult;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Scan QR Code'),
        backgroundColor:  Color(0xFF3FFF29),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: 16,
                vertical:10 ),
            child: Text(qrResult ?? ''),
          ),
          ElevatedButton(
            onPressed: () async {
              var res = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SimpleBarcodeScannerPage(),
                  ));
              setState(() {
                if (res is String) {
                  qrResult = res;
                  print('hasil $res');

                }
              });
            },
            child: const Text('Open Scanner'),
          ),
          // ElevatedButton(onPressed:(){
          //   Share.share('check out my website https://example.com');
          // }, child: Text("Share")),
          ElevatedButton(onPressed: (){
            FlutterClipboard.copy('$qrResult')
                .then(( value ) =>
                Fluttertoast.showToast(
                    msg:"${"copied"}",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Color(0xFF3FFF29),
                  fontSize: 16.0
                )
            );

          },
              child: Text('Copy'),
          style: ElevatedButton.styleFrom(
            backgroundColor:  Color(0xFF3FFF29)
          ),),

        ],
      ),
    );
  }
}
