import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class  ProfilePage extends StatefulWidget {
  const ProfilePage ({super.key});

  @override
  State <ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3FFF29),
        title: Text("KAYOYO",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold
          )),
      ),
      body: Column(
        children: [
          //Container(
            //height: 400,
          // width: double.infinity,
            //decoration: BoxDecoration(
             // image: DecorationImage(
                //image : AssetImage("C:\Users\T12\Documents\Kawo\ao_qr\images/ryou.jpg"),
              //)
           //),
         //),
          customForm(icon: Icon(Icons.add_card_rounded),
          hintText: "NIK",
          helperText:"Ini NIK"),


          customForm(icon: Icon(Icons.person),
          hintText: "Nama",
          helperText:"Nama Disini"),

          customForm(icon: Icon(Icons.email_outlined),
          hintText: "Email"),

          customForm(icon: Icon(Icons.home_filled),
          hintText: "Alamat"),

          ElevatedButton(
            onPressed: () {
              Fluttertoast.showToast(
                  msg: "This is Center Short Toast",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0
              );
            },
            child: Text("Simpan"),
            style:ElevatedButton.styleFrom(
                backgroundColor: Color(0xFF3FFF29),
                foregroundColor: Colors.white
            ) ,
          ),
        ],
      ),
    );
  }

  Padding customForm({required Icon icon, required String hintText,    String? helperText}) {
    return Padding(
          padding: const EdgeInsets.all(10.0),
          child: TextFormField(
            decoration: InputDecoration(
              helperText: helperText,
                hintText: hintText,
                prefixIcon: icon,
                suffixIcon: Icon(Icons.chevron_right),
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        width: 1,
                        color: Color(0xFF00FF83)
                    )
                )
            ),
          ),
        );
  }

}
