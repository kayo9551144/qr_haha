import 'package:ao_qr/page/profile_page.dart';
import 'package:ao_qr/page/scan_qr.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "KAYO",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Color(0xFF3FFF29),
      ),
      body: Column(
        children: [
          Text("EHHEHEHHEHEHHE"),
          Center(
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext ctx) => ProfilePage()));
              },
              child: Text("Simpan"),
              style: ElevatedButton.styleFrom(
                  backgroundColor: Color(0xFF3FFF29),
                  foregroundColor: Colors.white),
            ),
          ),
          ElevatedButton(onPressed: (){
            Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext ctx) => QR()));
          }, child:Text('Scan QR'))
        ],
      ),
      drawer: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width - 70,
        child: ListView(
          children: [
            Text("GMAIL"),
            Divider(),
            buildRow(IconData: Icons.all_inbox,
            text: Text("Semua Kotak Masuk")),
            Divider(),
            buildRow(IconData: Icons.inbox,
            text: Text("Kotak Masuk")),
            buildRow(IconData: Icons.people,
            text: Text("Sosial")),
            buildRow(IconData: Icons.star,
            text: Text("Promosi")),
            buildRow(IconData: Icons.schedule,
            text: Text("Snoozed")),
            buildRow(IconData: Icons.label_important_outline,
            text: Text("Important")),
            buildRow(IconData: Icons.send,
            text: Text("Send")),
            buildRow(IconData: Icons.schedule_send,
            text: Text("Scheduled")),
            buildRow(IconData: Icons.outbox,
            text: Text("Outbox")),
            buildRow(IconData: Icons.insert_drive_file,
            text: Text("Drafts")),
            buildRow(IconData: Icons.mail_outline,
            text: Text("Mail")),
            buildRow(IconData: Icons.report_gmailerrorred,
            text: Text("Spam")),
            buildRow(IconData: Icons.delete,
            text: Text("Trash")),
            Text("Google Apps"),
            Padding(padding: const EdgeInsets.only(bottom: 20.0)),
            buildRow(IconData: Icons.calendar_today,
            text: Text("Calendar")),
            buildRow(IconData: Icons.contacts_outlined,
            text: Text("Contacts")),
            Divider(),
            buildRow(IconData: Icons.settings,
            text: Text("Setting")),
            buildRow(IconData: Icons.help_outline,
            text: Text("Help & Feedback")),

          ],
        ),
      ),
    );
  }

  Row buildRow({IconData = IconData,required Text text}) {
    return Row(children: [
            Icon(IconData),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: text,
            )
          ]);
  }
}
